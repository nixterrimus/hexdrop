import UIKit

func clampTo<T: Comparable>(minimum minimum: T, maximum: T) -> (T -> T) {
    return { value in
        return max(minimum, min(maximum, value))
    }
}

func distanceBetweenCenters(view1: UIView, _ view2: UIView) -> CGSize {
    return CGSize(width: view2.center.x - view1.center.x, height: view1.center.y - view2.center.y)
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

public struct Theme {
    let background: UIColor
    let tint: UIColor
    let success: UIColor
    
    public static let standard: Theme = {
        return Theme(
            background: UIColor(netHex: 0x45598A),
            tint: UIColor(netHex: 0xEAEAEA),
            success: UIColor(netHex: 0x65AFFF)
        )
    }()
    
    public static let storm: Theme = {
        return Theme(
            background: UIColor(netHex: 0x002930),
            tint: UIColor(netHex: 0xF8F0AF),
            success: UIColor(netHex: 0xAC4A00)
        )
    }()
    
    public static let wwdc: Theme = {
        return Theme(
            background: UIColor(netHex: 0x20222B),
            tint: UIColor(netHex: 0x8485ce),
            success: UIColor(netHex: 0xd28e5d)
        )
    }()

}


class ViewController: UIViewController {
    var startingPoint: CGPoint!
    
    let theme: Theme = {
       return Theme.wwdc
    }()
    
    let hexagon: UIView = {
        let image = UIImage(named: "hex")!.imageWithRenderingMode(.AlwaysTemplate)
        return UIImageView(image: image)
    }()
    
    let plus: UIView = {
        let image = UIImage(named: "+")!.imageWithRenderingMode(.AlwaysTemplate)
        return UIImageView(image: image)
    }()
    
    let hexOutline: UIView = {
        let image = UIImage(named: "hex2")!.imageWithRenderingMode(.AlwaysTemplate)
        return UIImageView(image: image)
    }()
    
    override func viewDidLoad() {
        hexagon.frame.size = CGSize(width: 100, height: 100)
        hexOutline.frame.size = CGSize(width: 100, height
            : 100)
        plus.frame.size = CGSize(width: 80, height: 80)
        hexagon.isUserInteractionEnabled = true
        
        hexOutline.center.x = self.view.center.x/2
        hexOutline.center.y = 500
        plus.center = hexOutline.center
        
        hexagon.center.x = self.view.center.x/2
        hexagon.center.y = 120
        startingPoint = hexagon.center
        
        view.addSubview(hexagon)
        view.addSubview(plus)
        view.addSubview(hexOutline)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didPan(_:)))
        hexagon.addGestureRecognizer(panGesture)
        
        plus.alpha = 0
        
        self.view.backgroundColor = theme.background
        self.hexagon.tintColor = theme.tint
        self.plus.tintColor = theme.tint
        self.hexOutline.tintColor = theme.tint
        
        self.view.bringSubviewToFront(self.hexagon)
    }
    
    func opacity(distance: CGSize) -> CGFloat {
        let clamp = clampTo(minimum: CGFloat(0.4), maximum: 1.0)
        let xIntensity = clamp((250 - abs(distance.height)) / 200)
        let yIntensity = clamp((250 - abs(distance.width)) / 200)
        return min(xIntensity, yIntensity)
    }
    
    func size(distance: CGSize) -> CGAffineTransform {
        let clamp = clampTo(minimum: CGFloat(1.0), maximum: 1.3)
        let xScale = clamp((200 - abs(distance.height)) / 100)
        let yScale = clamp((200 - abs(distance.width)) / 100)
        let scale = min(xScale, yScale)
        
        return CGAffineTransformMakeScale(scale, scale)
    }
    
    func didPan(recognizer: UIPanGestureRecognizer){
        let location = recognizer.location(in: self.view)
        if let view = recognizer.view {
            view.center = location
        }
        
        plus.alpha = opacity(distance: distanceBetweenCenters(hexagon, hexOutline))
        hexOutline.transform = size(distance: distanceBetweenCenters(hexagon, hexOutline))
        
        self.hexagon.tintColor = theme.tint
        
        switch recognizer.state {
        case .Ended, .Cancelled:
            if self.hexOutline.frame.contains(self.hexagon.center){
                UIView.animate(
                    withDuration: 0.25,
                    delay: 0,
                    options: [],
                    animations: {
                        self.hexagon.center = self.hexOutline.center
                        self.plus.alpha = 0.0
                        self.hexOutline.transform = CGAffineTransform(1.1, 1.1)
                        self.hexagon.tintColor = self.theme.success
                        
                    },
                    completion: nil)
            } else {
                UIView.animateWithDuration(
                    0.25,
                    delay: 0,
                    options: .CurveEaseInOut,
                    animations: {
                        self.hexagon.center = self.startingPoint
                        self.plus.alpha = 0.0
                        self.hexOutline.transform = CGAffineTransform(1.0, 1.0)
                    },
                    completion: nil)
            }
        default:
            break
        }
    }
}

import PlaygroundSupport
let vc = ViewController()
PlaygroundPage.current.liveView = vc
