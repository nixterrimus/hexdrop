import UIKit

func clampTo<T: Comparable>(minimum minimum: T, maximum: T) -> (T -> T) {
    return { value in
        return max(minimum, min(maximum, value))
    }
}

func distanceBetweenCenters(view1: UIView, _ view2: UIView) -> CGSize {
    return CGSize(width: view2.center.x - view1.center.x, height: view1.center.y - view2.center.y)
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

public struct Theme {
    let background: UIColor
    let tint: UIColor
    let success: UIColor
    
    public static let standard: Theme = {
        return Theme(
            background: UIColor(netHex: 0x45598A),
            tint: UIColor(netHex: 0xEAEAEA),
            success: UIColor(netHex: 0x65AFFF)
        )
    }()
    
    public static let storm: Theme = {
        return Theme(
            background: UIColor(netHex: 0x002930),
            tint: UIColor(netHex: 0xF8F0AF),
            success: UIColor(netHex: 0xAC4A00)
        )
    }()
    
    public static let wwdc: Theme = {
        return Theme(
            background: UIColor(netHex: 0x20222B),
            tint: UIColor(netHex: 0x8485ce),
            success: UIColor(netHex: 0xd28e5d)
        )
    }()
    
}

class HexDropView: UIView {
    let plus: UIView = {
        let image = UIImage(named: "+")!.withRenderingMode(.alwaysTemplate)
        return UIImageView(image: image)
    }()
    
    let hexOutline: UIView = {
        let image = UIImage(named: "hex2")!.withRenderingMode(.alwaysTemplate)
        return UIImageView(image: image)
    }()
    
    override var tintColor: UIColor! {
        didSet {
            self.plus.tintColor = tintColor
            self.hexOutline.tintColor = tintColor
        }
    }

    override init(frame: CGRect){
        super.init(frame: frame)
        plus.frame.size = frame.insetBy(dx: 20, dy: 20).size
        hexOutline.frame.size = frame.size

        addSubview(plus)
        addSubview(hexOutline)
    }
    
    override func layoutSubviews() {
        plus.center = CGPoint(x: bounds.width/2, y: bounds.height/2)
        hexOutline.center = CGPoint(x: bounds.width/2, y: bounds.height/2)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class ViewController: UIViewController {
    var startingPoint: CGPoint!
    
    let theme: Theme = {
        return Theme.wwdc
    }()
    
    let hexagon: UIView = {
        let image = UIImage(named: "hex")!.withRenderingMode(.alwaysTemplate)
        return UIImageView(image: image)
    }()
    
    let hexdrops: [HexDropView] = {
        let depth: CGFloat = 400
        return [
            HexDropView(frame: CGRect(x: 10, y: depth, width: 100, height: 100)),
            HexDropView(frame: CGRect(x: 140, y: depth, width: 100, height: 100)),
            HexDropView(frame: CGRect(x: 270, y: depth, width: 100, height: 100)),
        ]
    }()
    
    
    override func viewDidLoad() {
        hexagon.frame.size = CGSize(width: 100, height: 100)
        hexagon.isUserInteractionEnabled = true
        
        hexagon.center.x = self.view.center.x/2
        hexagon.center.y = 120
        startingPoint = hexagon.center
        
        view.addSubview(hexagon)
        hexdrops.map { self.view.addSubview($0) }
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didPan(recognizer:)))
        hexagon.addGestureRecognizer(panGesture)
        
        hexdrops.forEach { hexdrop in
            hexdrop.plus.alpha = 0
        }

        
        self.view.backgroundColor = theme.background
        self.hexagon.tintColor = theme.tint
        hexdrops.forEach { hexdrop in
            hexdrop.tintColor = self.theme.tint
        }

        
        self.view.bringSubview(toFront: self.hexagon)
    }
    
    func opacity(distance: CGSize) -> CGFloat {
        let clamp = clampTo(minimum: CGFloat(0.4), maximum: 1.0)
        let xIntensity = clamp((250 - abs(distance.height)) / 200)
        let yIntensity = clamp((250 - abs(distance.width)) / 200)
        return min(xIntensity, yIntensity)
    }
    
    func size(distance: CGSize) -> CGAffineTransform {
        let clamp = clampTo(minimum: CGFloat(1.0), maximum: 1.3)
        let xScale = clamp((200 - abs(distance.height)) / 100)
        let yScale = clamp((200 - abs(distance.width)) / 100)
        let scale = min(xScale, yScale)
        
        return CGAffineTransform(scaleX: scale, y: scale)
    }
    
    func didPan(recognizer: UIPanGestureRecognizer){
        let location = recognizer.location(in: self.view)
        if let view = recognizer.view {
            view.center = location
        }
        
        
        hexdrops.forEach { hexdrop in
            hexdrop.plus.alpha = opacity(distance: distanceBetweenCenters(view1: hexagon, hexdrop))
            hexdrop.hexOutline.transform = size(distance: distanceBetweenCenters(view1: hexagon, hexdrop))
        }

        
        self.hexagon.tintColor = theme.tint
        
        switch recognizer.state {
        case .Ended, .Cancelled:
            
            let droppedOn = hexdrops.filter { hexdrop in
                return hexdrop.frame.contains(self.hexagon.center)
            }
            
            self.hexdrops
                .filter { $0 != droppedOn }
                .forEach { hexdrop in
                    UIView.animate(
                        withDuration: 0.12,
                        delay: 0,
                        options: [],
                        animations: {
                            hexdrop.hexOutline.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                            hexdrop.plus.alpha = 0.0
                        },
                        completion: nil)
                }
            
            if let hexdrop = droppedOn.first {
                UIView.animate(
                    withDuration: 0.25,
                    delay: 0,
                    options: [],
                    animations: {
                        self.hexagon.center = hexdrop.center
                        hexdrop.hexOutline.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                        self.hexagon.tintColor = self.theme.success
                        
                    },
                    completion: nil)
            } else {

                UIView.animate(
                    withDuration: 0.25,
                    delay: 0,
                    options: [],
                    animations: {
                        self.hexagon.center = self.startingPoint
                    },
                    completion: nil)
            }
        default:
            break
        }
    }
}

import PlaygroundSupport
let vc = ViewController()
PlaygroundPage.current.liveView = vc
